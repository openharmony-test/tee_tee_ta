/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ADAPTOR_LOG_H
#define ADAPTOR_LOG_H

#include "tee_log.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define HDF_LOG_TAG PIN_AUTH_HDI

#ifndef OHOS_DEBUG
#define DECORATOR_TALOG(op, fmt, args...)             \
    do {                                               \
        op("%s() " fmt, __FUNCTION__, ##args); \
    } while (0)
#else
#define DECORATOR_TALOG(op, fmt, args...)                                     \
    do {                                                                       \
        op("{%s()-%s:%d} " fmt, __FUNCTION__, __LINE__, ##args); \
    } while (0)
#endif

#define LOG_ERROR(fmt, ...) DECORATOR_TALOG(tloge, fmt, ##__VA_ARGS__)
#define LOG_INFO(fmt, ...) DECORATOR_TALOG(tlogi, fmt, ##__VA_ARGS__)

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // ADAPTOR_LOG_H